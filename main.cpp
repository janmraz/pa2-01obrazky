#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG = 0x4d4d;

#endif /* __PROGTEST__ */

enum color_type {
    blacknwhite,
    rgb,
    rgba
};

enum color_size {
    oneBit,
    eightBit,
    sixteenBit
};

struct PixelRepresentation {
    uint8_t data[8];
};

class MirroringManager {
    ifstream m_inputFS;
    ofstream m_outputFS;
    bool m_littleEndian;
    uint16_t m_width;
    uint16_t m_height;
    color_type m_color_type;
    color_size m_color_size;
    PixelRepresentation **m_data;
    int m_size;
public:
    MirroringManager(const char *srcFileName,
                     const char *dstFileName);

    ~MirroringManager();

    bool initLoading();
    bool initWriting();

    void verticalFlip();
    void horizontalFlip();

    void cleanUp();

private:
    // reading byte helpers
    uint8_t readByte();
    uint16_t load2BytesValue();

    // convertors from number color representation
    color_size getColorSize(uint16_t input);
    color_type getColorType(uint16_t input);

    // header helpers
    bool loadHeader();
    void writeHeader();

    bool checkFile();

    // load one representation into memory
    PixelRepresentation loadOneBit();
    PixelRepresentation loadEightBit();
    PixelRepresentation loadSixteenBit();
    PixelRepresentation loadBit();

    int getNumberOfIterations();

    // write helpers for
    void write(uint8_t data);
    void write(uint16_t data);

};

MirroringManager::MirroringManager(const char *srcFileName,
                                   const char *dstFileName) {
    // initializing streams
    m_inputFS = ifstream(srcFileName, ifstream::ate | ios::in | ios::binary);
    m_outputFS = ofstream(dstFileName, ios::out | ios::binary);

    // getting size of input source and pointer at the beginning
    m_size = m_inputFS.tellg();
    m_inputFS.seekg(0);
}

MirroringManager::~MirroringManager() {
    // closing output stream
    m_outputFS.close();
    m_inputFS.close();
}

/**
 * Loads one byte only
 *
 * @returns loaded byte value
 */
uint8_t MirroringManager::readByte() {
    uint8_t input;
    m_inputFS.read(reinterpret_cast<char *>(&input), 1);
    return input;
}

/**
 * Checks if streams are errorless
 *
 * @returns true if streams doesnt contain any error
 */
bool MirroringManager::checkFile() {
    return m_inputFS.good() && m_outputFS.good();
}

/**
 * Loads two bytes (shift them if big endian)
 *
 * @returns loaded 2byte value
 */
uint16_t MirroringManager::load2BytesValue() {
    uint8_t lowerBit;
    uint8_t higherBit;

    if (m_littleEndian) {
        lowerBit = readByte();
        higherBit = readByte();
    } else {
        higherBit = readByte();
        lowerBit = readByte();
    }

    return (higherBit << 8) | lowerBit;
}

/**
 * Returns color size based on two byte input
 *
 *  @throws invalidColorSize exception if color size is in invalid format
 * @returns value from color size enum
 */
color_size MirroringManager::getColorSize(uint16_t input) {
    int shiftedValue = input & 28;
    switch (shiftedValue) {
        case 0:
            return oneBit;
        case 12:
            return eightBit;
        case 16:
            return sixteenBit;
        default:
            throw "invalidColorSize";
    }
}

/**
 * Returns color type based on two byte input
 *
 *  @throws invalidColorType exception if color type is in invalid format
 * @returns value from color type enum
 */
color_type MirroringManager::getColorType(uint16_t input) {
    int shiftedValue = input & 3;
    switch (shiftedValue) {
        case 0:
            return blacknwhite;
        case 2:
            return rgb;
        case 3:
            return rgba;
        default:
            throw "invalidColorType";
    }
}

/**
 * Loads header, determine endianity and save width, height, color size and color type.
 *
 * Header consists of:
 *  1. endianity represented 0x4949 (little endian) or 0x4d4d( big endian);
 *  2. width of source image
 *  3. height of source image
 *  4. color representation
 *      4.1 lowest 2 bits represents size of color (such as eight bit or sixteen bit)
 *      4.2 next 3 bits represents color type (such as rgb or rgba)
 *
 * @returns Pixel representation
 */
bool MirroringManager::loadHeader() {
    // read first two bytes and check whether it is little endian
    uint8_t endianByte1 = readByte();
    uint8_t endianByte2 = readByte();
    if (endianByte1 == 73 && endianByte2 == 73) {
        m_littleEndian = true;
    } else if (endianByte1 == 77 && endianByte2 == 77) {
        m_littleEndian = false;
    } else {
        return false;
    }

    m_width = load2BytesValue();
    m_height = load2BytesValue();

    if (m_width <= 0 || m_height <= 0)
        return false;

    uint16_t size = load2BytesValue();
    try {
        m_color_size = getColorSize(size);
        m_color_type = getColorType(size);
    } catch (...) {
        return false;
    }

    // check if size of input and expected size are same otherwise return false
    int iterations = getNumberOfIterations();
    int expectedSize = (m_width * m_height * iterations) + 8;
    if (expectedSize != m_size)
        return false;

    // init data representation
    m_data = new PixelRepresentation *[m_height];
    for (int i = 0; i < m_height; ++i)
        m_data[i] = new PixelRepresentation[m_width];

    return true;
}

PixelRepresentation MirroringManager::loadOneBit() {
    throw "UNIMPLEMENTED";
}

/**
 * Loads one byte represented as eight bits
 *
 * @returns Pixel representation
 */
PixelRepresentation MirroringManager::loadEightBit() {
    PixelRepresentation pixel;
    pixel.data[0] = readByte();
    if (m_color_type == rgb || m_color_type == rgba) {
        pixel.data[1] = readByte();
        pixel.data[2] = readByte();
    }
    if (m_color_type == rgba)
        pixel.data[3] = readByte();
    return pixel;
}

/**
 * Loads one byte represented as sixteen bits
 *
 * @returns Pixel representation
 */
PixelRepresentation MirroringManager::loadSixteenBit() {
    PixelRepresentation pixel;
    pixel.data[0] = readByte();
    pixel.data[1] = readByte();
    if (m_color_type == rgb || m_color_type == rgba) {
        pixel.data[2] = readByte();
        pixel.data[3] = readByte();
        pixel.data[4] = readByte();
        pixel.data[5] = readByte();
    }
    if (m_color_type == rgba) {
        pixel.data[6] = readByte();
        pixel.data[7] = readByte();
    }
    return pixel;
}

/**
 * Loads one byte based on color size
 *
 * @throws invalidColorSize exception if color size is in invalid format
 * @returns Pixel representation
 */
PixelRepresentation MirroringManager::loadBit() {
    switch (m_color_size) {
        case eightBit:
            return loadEightBit();
        case sixteenBit:
            return loadSixteenBit();
        case oneBit:
            return loadOneBit();
    }
    throw "invalidColorSize";
}

/**
 * Returns number of iteration based on data representation.
 *
 * @throws invalidColorSize exception if color size is in invalid format
 * @returns number
 */
int MirroringManager::getNumberOfIterations() {
    if (m_color_size == oneBit) {
        throw "UNIMPLEMENTED";
    } else if (m_color_size == eightBit) {
        switch (m_color_type) {
            case blacknwhite:
                return 1;
            case rgb:
                return 3;
            case rgba:
                return 4;
        }
    } else if (m_color_size == sixteenBit) {
        switch (m_color_type) {
            case blacknwhite:
                return 2;
            case rgb:
                return 6;
            case rgba:
                return 8;
        }
    }
    throw "invalidColorSize";
}

/**
 * Loads data from input stream and store them
 *
 * @returns true if success
 */
bool MirroringManager::initLoading() {
    if (!checkFile())
        return false;
    if (!m_inputFS.is_open())
        return false;
    if (!loadHeader())
        return false;
    for (int i = 0; i < m_height; i++)
        for (int j = 0; j < m_width; j++)
            m_data[i][j] = loadBit();

    return !m_inputFS.bad();
}

/**
 * Entry point for writing responsible for writing data
 *
 * @returns true if success
 */
bool MirroringManager::initWriting() {
    if (!checkFile())
        return false;
    if (!m_outputFS.is_open())
        return false;
    writeHeader();

    int numberOfIteration = getNumberOfIterations();
    for (int i = 0; i < m_height; i++)
        for (int j = 0; j < m_width; j++)
            for (int k = 0; k < numberOfIteration; k++)
                write((u_int8_t) m_data[i][j].data[k]);
    return !m_outputFS.bad();
}

/**
 * Cleaning up after job is done
 */
void MirroringManager::cleanUp(){
    for (int i = 0; i < m_height; i++)
        delete[]m_data[i];
    delete[] m_data;
}

/**
 * Writes header into file based on endianity
 *
 * Header consists of:
 *  1. endianity
 *  2. width
 *  3. height
 *  4. color representaion (where lowest 2 bits represents size of color and another 3 bits represents type representaion )
 *      4.1 lowest 2 bits represents size of color (such as eight bit or sixteen bit)
 *      4.2 next 3 bits represents color type (such as rgb or rgba)
 */
void MirroringManager::writeHeader() {
    if (m_littleEndian) {
        write((uint16_t) ENDIAN_LITTLE);
        write((uint16_t) m_width);
        write((uint16_t) m_height);
        int colorType = m_color_type == rgb ? 2 : (m_color_type == rgba ? 3 : 0);
        int colorSize = m_color_size == oneBit ? 0 : (m_color_size == eightBit ? 12 : 16);
        write((uint16_t) (colorSize + colorType));
    } else {
        write((uint16_t) ENDIAN_BIG);
        write((uint16_t) (m_width << 8));
        write((uint16_t) (m_height << 8));
        int colorType = m_color_type == rgb ? 2 : (m_color_type == rgba ? 3 : 0);
        int colorSize = m_color_size == oneBit ? 0 : (m_color_size == eightBit ? 12 : 16);
        write((uint16_t) ((colorSize + colorType) << 8));
    }

}


/**
 * Flip stored bytes vertically
 */
void MirroringManager::verticalFlip() {
    for (int i = 0; i < m_height / 2; i++) {
        PixelRepresentation *tmp = m_data[i];
        m_data[i] = m_data[m_height - i - 1];
        m_data[m_height - i - 1] = tmp;
    }
}

/**
 * Flip stored bytes horizontally
 */
void MirroringManager::horizontalFlip() {
    for (int i = 0; i < m_height; i++) {
        for (int j = 0; j < m_width / 2; j++) {
            PixelRepresentation tmp = m_data[i][j];
            m_data[i][j] = m_data[i][m_width - j - 1];
            m_data[i][m_width - j - 1] = tmp;
        }
    }
}

/**
 * Writes two bytes into the stream
 *
 * @param data two bytes which will be written
 */
void MirroringManager::write(uint16_t data) {
    m_outputFS.write((const char *) &data, sizeof(data));
}

/**
 * Writes one bytes into the stream
 *
 * @param data byte which will be written
 */
void MirroringManager::write(uint8_t data) {
    m_outputFS.write((const char *) &data, sizeof(data));
}


bool flipImage(const char *srcFileName,
               const char *dstFileName,
               bool flipHorizontal,
               bool flipVertical) {
    MirroringManager mirroringManager(srcFileName, dstFileName);
    bool resultOk;

    //  start loading image into memory
    resultOk = mirroringManager.initLoading();
    if (!resultOk)
        return false;

    //  do transformations on image
    if (flipHorizontal)
        mirroringManager.horizontalFlip();
    if (flipVertical)
        mirroringManager.verticalFlip();

    //  start writing output image from memory
    resultOk = mirroringManager.initWriting();
    mirroringManager.cleanUp();
    return resultOk;
}

#ifndef __PROGTEST__

bool identicalFiles(const char *fileName1,
                    const char *fileName2) {
}

int main(void) {
    assert (flipImage("input_00.img", "output_00.img", true, false)
            && identicalFiles("output_00.img", "ref_00.img"));

    assert (flipImage("input_01.img", "output_01.img", false, true)
            && identicalFiles("output_01.img", "ref_01.img"));

    assert (flipImage("input_02.img", "output_02.img", true, true)
            && identicalFiles("output_02.img", "ref_02.img"));

    assert (flipImage("input_03.img", "output_03.img", false, false)
            && identicalFiles("output_03.img", "ref_03.img"));

    assert (flipImage("input_04.img", "output_04.img", true, false)
            && identicalFiles("output_04.img", "ref_04.img"));

    assert (flipImage("input_05.img", "output_05.img", true, true)
            && identicalFiles("output_05.img", "ref_05.img"));

    assert (flipImage("input_06.img", "output_06.img", false, true)
            && identicalFiles("output_06.img", "ref_06.img"));

    assert (flipImage("input_07.img", "output_07.img", true, false)
            && identicalFiles("output_07.img", "ref_07.img"));

    assert (flipImage("input_08.img", "output_08.img", true, true)
            && identicalFiles("output_08.img", "ref_08.img"));

    assert (!flipImage("input_09.img", "output_09.img", true, false));

    // extra inputs (optional & bonus tests)
    assert (flipImage("extra_input_00.img", "extra_out_00.img", true, false)
            && identicalFiles("extra_out_00.img", "extra_ref_00.img"));
    assert (flipImage("extra_input_01.img", "extra_out_01.img", false, true)
            && identicalFiles("extra_out_01.img", "extra_ref_01.img"));
    assert (flipImage("extra_input_02.img", "extra_out_02.img", true, false)
            && identicalFiles("extra_out_02.img", "extra_ref_02.img"));
    assert (flipImage("extra_input_03.img", "extra_out_03.img", false, true)
            && identicalFiles("extra_out_03.img", "extra_ref_03.img"));
    assert (flipImage("extra_input_04.img", "extra_out_04.img", true, false)
            && identicalFiles("extra_out_04.img", "extra_ref_04.img"));
    assert (flipImage("extra_input_05.img", "extra_out_05.img", false, true)
            && identicalFiles("extra_out_05.img", "extra_ref_05.img"));
    assert (flipImage("extra_input_06.img", "extra_out_06.img", true, false)
            && identicalFiles("extra_out_06.img", "extra_ref_06.img"));
    assert (flipImage("extra_input_07.img", "extra_out_07.img", false, true)
            && identicalFiles("extra_out_07.img", "extra_ref_07.img"));
    assert (flipImage("extra_input_08.img", "extra_out_08.img", true, false)
            && identicalFiles("extra_out_08.img", "extra_ref_08.img"));
    assert (flipImage("extra_input_09.img", "extra_out_09.img", false, true)
            && identicalFiles("extra_out_09.img", "extra_ref_09.img"));
    assert (flipImage("extra_input_10.img", "extra_out_10.img", true, false)
            && identicalFiles("extra_out_10.img", "extra_ref_10.img"));
    assert (flipImage("extra_input_11.img", "extra_out_11.img", false, true)
            && identicalFiles("extra_out_11.img", "extra_ref_11.img"));
    return 0;
}

#endif /* __PROGTEST__ */
